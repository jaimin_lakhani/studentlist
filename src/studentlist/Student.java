package studentlist;

/**
 * This class represents students in our application
 *
 * @author Paul Bonenfant
 */
public class Student {
    
    private String name;
    private String program;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getPogram() {
        return program;
    }
    
    public void setProgram() {
        this.program = program;
    }

}

